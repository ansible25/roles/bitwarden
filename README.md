Bitwarden
=========

Role that installs Bitwarden

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Installing Bitwarden on targeted machine:

    - hosts: servers
      roles:
         - bitwarden

License
-------

[MIT](LICENSE)

Author Information
------------------

This role was created by [bradthebuilder](https://bradthebuilder.me)
